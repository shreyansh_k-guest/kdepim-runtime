When updating this repo from upstream; also copy tags otherwise the builds might fail.

To copy tags from upstream:
```
$ git remote add upstream ... # to add upstream
$ git fetch --tags upstream
$ git push --tags origin
```
